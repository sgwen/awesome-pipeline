describe('test frontend', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/')
  })

  it('trajet Paris-Bruxelles pour la date du jour', () => {
    cy.get('#departure').type('Paris')
    cy.get('#arrival').type('Bruxelles')
    cy.get('#search').click()
    cy.get('#results').should('contain', 'Book')
  })

  it('réservation', () => {
    cy.get('#departure').type('Paris')
    cy.get('#arrival').type('Bruxelles')
    cy.get('#search').click()
    cy.get('#results > :nth-child(1)').contains('Book').click()
    cy.get("#cart").should('contain', 'My cart')
  })

  it('supprimer élément du panier', () => {
    cy.get('[href="cart.html"]').click()
    cy.get("#cart").should('contain', 'My cart')
    cy.get('#cart > :nth-child(2) > button[class="delete"]').click()
  })

  it('achat', () => {
    cy.get('[href="cart.html"]').click()
    cy.get("#cart").should('contain', 'My cart')
    cy.get('#purchase').click()
    cy.get('#trips').should('contain', 'My bookings')
  })
})