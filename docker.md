Exercice bonus dockerfile

Reprenez les deux dossiers du hackathon backend-main et frontend-main
Dans le dossier backend-main, créez un Dockerfile afin de créer une image permettant à votre backend de fonctionner.
Indices:

- partez d'une image node:alpine
    <details>FROM node:alpine </details>

- créez un fichier .dockerignore afin d'exclure le fichier .env ainsi que le dossier node_modules (même syntaxe qu'un fichier .gitignore)

- vous aurez besoin de vous positionner dans le dossier /home/node/app dans votre container pour installer les dépendances (WORKDIR)
    <details>WORKDIR /home/node/app</details>

- vous aurez besoin de déclarer un argument CONNECTION_STRING dans votre Dockerfile (ARG)
    <details>ARG CONNECTION_STRING</details>

- vous devrez copier le contenu de votre dossier courant dans le working dir du container (COPY)
    <details>COPY ./ ./</details>

- votre instruction RUN devra créer le fichier .env puis y inscrire "CONNECTION_STRING=$CONNECTION_STRING", vous aurez stocké le contenu de votre connection string dans une variable d'environnement CONNECTION_STRING que vous passerez à la commande de build. (indice: --build-arg)
- votre instruction RUN devra aussi installer les dépendances de l'application
    <details><summary>instruction RUN</summary>
    RUN touch .env \
    && echo "CONNECTION_STRING=$CONNECTION_STRING" >> .env \
    && yarn install
    </details>

- vous devrez exposer le port 3000 de votre container
    <details>EXPOSE 3000</details>

- l'instruction CMD devra lancer votre backend
    <details>CMD ["yarn", "start"]</details>

Construisez votre image
    <details><summary>commande de build</summary>
    docker build --build-arg CONNECTION_STRING=$CONNECTION_STRING -t tickethack-backend .
    </details>

lorsque vous lancerez un container avec cette image, n'oubliez pas de binder le port 3000 de votre machine au port 3000 du container (voir exercice nginx)
    <details>docker run -p 3000:3000 tickethack-backend</details>


Dans le dossier frontend-main, créez un Dockerfile et un .dockerignore également (pour exclure le dossier node_modules encore une fois)
Le Dockerfile devra lancer votre frontend.
Indices:
- partez d'une image node:alpine
    <details>FROM node:alpine </details>

- même WORKDIR que pour le backend (/home/node/app)
    <details>WORKDIR /home/node/app</details>

- vous devrez copier le contenu de votre dossier courant dans le working dir du container (COPY)
    <details>COPY ./ ./</details>

- votre instruction RUN devra installer globalement http-server puis installer les dépendances
    <details>RUN npm install -g http-server \
    && npm install</details>

- vous devrez exposer le port 8080 de votre container
    <details>EXPOSE 8080</details>

- l'instruction CMD devra lancer http-server
    <details>CMD ["http-server"]</details>

Construisez votre image
    <details><summary>commande de build</summary>
    docker build -t tickethack-frontend .
    </details>

pour lancer un container avec cette image, n'oubliez pas également de binder le port 8080 du container à un port de votre machine (par exemple le 8080 également)
    <details>docker run -p 8080:8080 tickethack-frontend</details>

Lancez les deux containers puis visitez l'adresse localhost:8080... Voilà, vous avez déployé une application dans des containers!
